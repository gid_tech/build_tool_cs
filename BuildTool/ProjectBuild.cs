﻿//	using System;
//	using System.Collections.Generic;
//	using System.IO;
//	using System.Linq;
//	using System.Threading;
//	using System.Xml.Serialization;

using BuildTool.Config;
using System.Xml.Serialization;

namespace BuildTool
{
	public static bool Build(string rootPath, BuildConfig buildConfig, Timer timer)
	{
		var serializer = new XmlSerializer(typeof(Project));
		
	}

	//	public delegate void BuildCommand();

	//	public static class ProjectBuild
	//	{
	//		public static bool Build(string rootProjectPath, BuildConfig buildConfig, Timer timer)
	//		{
	//			var projectBuilds = new List<ProjectPartBuild>();

	//			void CollectProjectPartBuilds(string projectPath)
	//			{
	//				if (!projectBuilds.Any(p => p.ProjectPath == projectPath))
	//				{
	//					var projectDir = Path.GetDirectoryName(projectPath)!;
	//					Project project;
	//					{
	//						using var file = File.OpenRead(projectPath);
	//						project = (serializer.Deserialize(file) as Project) ?? throw new Exception();
	//					}

	//					var projectPartBuild = project.Parts switch
	//					{
	//						CopyProjectPart[] cs => new CopyProjectPartBuild(cs, buildConfig, projectPath, rootProjectPath),
	//						GlslProjectPart[] cs => new GlslProjectPartBuild(cs, buildConfig, projectPath, rootProjectPath),
	//						CppProjectPart[] cs => new CppProjectPartBuild(cs, buildConfig, projectPath, rootProjectPath),
	//						RustProjectPart[] cs => new RustProjectPartBuild(cs, buildConfig, projectPath, rootProjectPath),
	//						OdinProjectPart[] cs => new OdinProjectPartBuild(cs, buildConfig, projectPath, rootProjectPath),
	//						ProjectPart[] cs => new ProjectPartBuild(cs, buildConfig, projectPath, rootProjectPath),
	//						_ => throw new Exception(),
	//					};

	//					projectBuilds.Add(projectPartBuild);

	//					foreach (var subProject in projectPartBuild.SubProjects)
	//						CollectProjectPartBuilds(subProject);
	//				}
	//			}

	//			CollectProjectPartBuilds(rootProjectPath);

	//			var errorHappend = false;
	//			var logLock = new object();
	//			var threads = projectBuilds.Select(projectPartBuild =>
	//			{
	//				var thread = new Thread(() =>
	//				{
	//					foreach (var subProject in projectPartBuild.SubProjects)
	//						projectBuilds.First(p => p.ProjectPath == subProject).IsDone.WaitOne();

	//					(int l, int t) logCursorPosition;
	//					lock (logLock)
	//					{
	//						Console.ForegroundColor = ConsoleColor.Blue;
	//						Console.Write($"started building: {Path.GetRelativePath(Path.GetDirectoryName(rootProjectPath)!, projectPartBuild.ProjectPath)}");
	//						logCursorPosition = Console.GetCursorPosition();
	//						Console.ResetColor();
	//						Console.WriteLine();
	//					}

	//					var timerTask = new Timer.Task(projectPartBuild.ProjectPath, "building");

	//					BuildResults result;
	//					if (!errorHappend)
	//					{
	//						result = projectPartBuild.Build();
	//						if (result == BuildResults.Failed)
	//							errorHappend = true;
	//					}
	//					else
	//					{
	//						result = BuildResults.Skipped;
	//					}

	//					lock (timer)
	//						timerTask.Submit(timer);

	//					lock (logLock)
	//					{
	//						ConsoleColor ResultToColor() => result switch
	//						{
	//							BuildResults.Failed => ConsoleColor.Red,
	//							BuildResults.Succeeded => ConsoleColor.Green,
	//							BuildResults.Skipped => ConsoleColor.Yellow,
	//							_ => throw new NotImplementedException(),
	//						};
	//						string ResultToString() => result switch
	//						{
	//							BuildResults.Failed => "failed",
	//							BuildResults.Succeeded => "succeeded",
	//							BuildResults.Skipped => "skipped",
	//							_ => throw new NotImplementedException(),
	//						};

	//						var (left, top) = Console.GetCursorPosition();
	//						Console.SetCursorPosition(logCursorPosition.l, logCursorPosition.t);
	//						Console.Write(" : ");
	//						Console.ForegroundColor = ResultToColor();
	//						Console.WriteLine(ResultToString());
	//						Console.ResetColor();
	//						Console.SetCursorPosition(left, top);
	//					}

	//					projectPartBuild.IsDone.Set();
	//				});
	//				thread.Start();
	//				thread.Name = projectPartBuild.ProjectName;
	//				return thread;
	//			}).ToArray();

	//			foreach (var thread in threads)
	//				thread!.Join();

	//			return !errorHappend;
	//		}
	//	}
}
