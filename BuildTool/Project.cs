﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace BuildTool.Config
{
	public class Filtered
	{
		[XmlAttribute]
		public string? Config = null;
		[XmlAttribute]
		public string? Platform = null;
	}

	public class Arg : Filtered
	{
		[XmlText]
		public string Value = string.Empty;
	}

	public class Dir : Filtered
	{
		[XmlText]
		public string Value = string.Empty;
	}

	public class Ending : Filtered
	{
		[XmlText]
		public string Value = string.Empty;
	}

	public class File : Filtered
	{
		[XmlText]
		public string Value = string.Empty;
	}

	public class Project
	{
		public File[] CppFiles = Array.Empty<File>();
		public Dir[] CppDirs = Array.Empty<Dir>();
		
		public Dir[] IncludeDirs = Array.Empty<Dir>();
		public Arg[] Args = Array.Empty<Arg>();
		public Arg[] CompileArgs = Array.Empty<Arg>();
		public Arg[] LinkArgs = Array.Empty<Arg>();

		public File[] GlslFiles = Array.Empty<File>();
		public Dir[] GlslDirs = Array.Empty<Dir>();

		public File[] CopyFiles = Array.Empty<File>();
		public Dir[] CopyDirs = Array.Empty<Dir>();
	}
}
