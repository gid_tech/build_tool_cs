﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static BuildTool.ProjectPartBuildUtils;

namespace BuildTool
{
	internal enum BuildResults
	{
		Failed, Succeeded, Skipped
	}

	//	internal class ProjectPartBuild
	//	{
	//		public readonly ManualResetEvent IsDone = new ManualResetEvent(false);

	//		public readonly BuildConfig BuildConfig;

	//		public readonly string ProjectPath; 
	//		public readonly string ProjectDir;
	//		public readonly string ProjectName;

	//		public readonly string RootProjectPath;
	//		public readonly string RootProjectDir;
	//		public readonly string RootProjectName;

	//		public readonly string RelativePath;

	//		public readonly Dictionary<string, string> Replacements;

	//		public readonly string[] SubProjects;

	//		public ProjectPartBuild(ProjectPart[] projectParts, BuildConfig buildConfig, string projectPath, string rootProjectPath)
	//		{
	//			projectParts = projectParts.Filter(buildConfig);

	//			this.BuildConfig = buildConfig;

	//			this.ProjectPath = projectPath;
	//			this.ProjectDir = Path.GetDirectoryName(projectPath)!;
	//			this.ProjectName = Path.GetFileNameWithoutExtension(projectPath);

	//			this.RootProjectPath = rootProjectPath;
	//			this.RootProjectDir = Path.GetDirectoryName(rootProjectPath)!;
	//			this.RootProjectName = Path.GetFileNameWithoutExtension(rootProjectPath);

	//			this.RelativePath = Path.GetRelativePath(this.RootProjectDir, this.ProjectDir);

	//			this.Replacements = new Dictionary<string, string>
	//			{
	//				{ nameof(this.ProjectName), this.ProjectName },
	//				{ nameof(this.ProjectDir), this.ProjectDir },
	//				{ nameof(this.RootProjectName), this.RootProjectName },
	//				{ nameof(this.RootProjectDir), this.RootProjectDir },
	//				{ nameof(this.RelativePath), this.RelativePath },
	//				{ nameof(buildConfig.Platform), $"{buildConfig.Platform}"},
	//				{ nameof(buildConfig.Config), buildConfig.Config },
	//				{ nameof(buildConfig.TempDir), buildConfig.TempDir },
	//				{ nameof(buildConfig.TargetDir), buildConfig.TargetDir },
	//			};

	//			this.SubProjects = ReplacePath(projectParts.Select(s => s.SubProjects), this.Replacements);
	//		}

	//		public BuildResults Build()
	//		{
	//			var result = this.BuildImpl();
	//			this.IsDone.Set();
	//			return result;
	//		}

	//		protected virtual BuildResults BuildImpl() => BuildResults.Succeeded;
	//	}

	//	internal class CopyProjectPartBuild : ProjectPartBuild
	//	{
	//		public readonly string SrcDir;
	//		public readonly string DstDir;
	//		public readonly string[] FileEndings;

	//		public CopyProjectPartBuild(CopyProjectPart[] projectParts, BuildConfig buildConfig, string projectPath, string rootProjectPath) :
	//			base(projectParts, buildConfig, projectPath, rootProjectPath)
	//		{
	//			projectParts = projectParts.Filter(buildConfig);

	//			this.SrcDir = ReplacePath(projectParts.Select(s => s.SrcDir), this.Replacements);
	//			this.DstDir = ReplacePath(projectParts.Select(s => s.DstDir), this.Replacements);
	//			this.FileEndings = Replace(projectParts.Select(s => s.FileEndings), this.Replacements);
	//		}

	//		protected override BuildResults BuildImpl()
	//		{
	//			var allFiles = this.FileEndings
	//				.SelectMany(e => FileUtils.FindFiles(this.SrcDir, e))
	//				.Select(originalFile =>
	//				{
	//					var copyDir = Path.GetRelativePath(this.ProjectDir, Path.GetDirectoryName(originalFile)!);
	//					var copyName = Path.GetFileName(originalFile);
	//					var copyFile = Path.GetFullPath(Path.Join(this.DstDir, copyDir, copyName));
	//					return (originalFile, copyFile);
	//				})
	//				.ToArray();

	//			var filesToCopy = allFiles
	//				.Where(f => FileUtils.YoungestFile(new[] { f.originalFile, f.copyFile, this.ProjectPath }) != f.copyFile)
	//				.ToArray();

	//			Console.WriteLine(string.Join("\n", filesToCopy.Select(f => $"copying: {Path.GetRelativePath(this.ProjectDir, f.originalFile)}")));
	//			foreach (var (originalFile, copyFile) in filesToCopy)
	//			{
	//				Directory.CreateDirectory(Path.GetDirectoryName(copyFile)!);
	//				File.Copy(originalFile, copyFile, true);
	//				File.SetLastWriteTime(copyFile, DateTime.Now);
	//			}

	//			return filesToCopy.Length > 0 ? BuildResults.Succeeded : BuildResults.Skipped;
	//		}
	//	}

	//	internal class GlslProjectPartBuild : ProjectPartBuild
	//	{
	//		public readonly string SrcDir;
	//		public readonly string DstDir;
	//		public readonly string[] Args;

	//		public GlslProjectPartBuild(GlslProjectPart[] projectParts, BuildConfig buildConfig, string projectPath, string rootProjectPath) :
	//			base(projectParts, buildConfig, projectPath, rootProjectPath)
	//		{
	//			this.SrcDir = ReplacePath(projectParts.Select(s => s.SrcDir), this.Replacements);
	//			this.DstDir = ReplacePath(projectParts.Select(s => s.DstDir), this.Replacements);
	//			this.Args = Replace(projectParts.Select(s => s.Args), this.Replacements);
	//		}

	//		protected override BuildResults BuildImpl()
	//		{
	//			var allFiles =
	//				 FileUtils.FindFiles(this.SrcDir, ".frag").Union(FileUtils.FindFiles(this.SrcDir, ".vert"))
	//				.Select(glslFile =>
	//				{
	//					var spvDir = Path.GetRelativePath(this.ProjectDir, Path.GetDirectoryName(glslFile)!);
	//					var spvName = Path.GetFileName(glslFile) + ".spv";
	//					var spvFile = Path.Join(this.DstDir, spvDir, spvName);
	//					return (glslFile, spvFile);
	//				}).ToArray();

	//			var filesToCompile = allFiles
	//				.Where(f => FileUtils.YoungestFile(
	//					new[] { f.glslFile, f.spvFile, this.ProjectPath }.Union(FileUtils.FindDeps(f.glslFile, Array.Empty<string>()))) != f.spvFile
	//				).ToArray();

	//			Console.WriteLine(string.Join("\n", filesToCompile.Select(f => $"compiling: {Path.GetRelativePath(this.ProjectDir, f.glslFile)}")));

	//			var errorHappened = false;
	//			Parallel.ForEach(filesToCompile, (f, _, __) =>
	//			{
	//				Directory.CreateDirectory(Path.GetDirectoryName(f.spvFile)!);
	//				var process = new Process()
	//				{
	//					StartInfo = new ProcessStartInfo
	//					{
	//						WorkingDirectory = ProjectDir,
	//						FileName = "glslc",
	//						Arguments =
	//							$"{f.glslFile} " +
	//							$"{string.Join(" ", this.Args)} " +
	//							$"-o {f.spvFile}",
	//					}
	//				};
	//				process.Start();
	//				process.WaitForExit();
	//				if (process.ExitCode != 0)
	//					errorHappened = true;
	//			});

	//			return errorHappened
	//				? BuildResults.Failed
	//				: filesToCompile.Length > 0 ? BuildResults.Succeeded : BuildResults.Skipped;
	//		}
	//	}

	//	internal class CppProjectPartBuild : ProjectPartBuild
	//	{
	//		public readonly string[] SrcDirs;
	//		public readonly string[] LibFiles;
	//		public readonly string DstAssembly;
	//		public readonly string[] IncludeDirectories;
	//		public readonly string[] Args;
	//		public readonly string[] CompileArgs;
	//		public readonly string[] LinkArgs;

	//		public CppProjectPartBuild(CppProjectPart[] projectParts, BuildConfig buildConfig, string projectPath, string rootProjectPath) :
	//			base(projectParts, buildConfig, projectPath, rootProjectPath)
	//		{
	//			projectParts = projectParts.Filter(buildConfig);

	//			this.SrcDirs = ReplacePath(projectParts.Select(s => s.SrcDirs), this.Replacements);
	//			this.LibFiles = ReplacePath(projectParts.Select(s => s.LibFiles), this.Replacements);
	//			this.DstAssembly = ReplacePath(projectParts.Select(s => s.DstAssembly), this.Replacements);
	//			this.IncludeDirectories = ReplacePath(projectParts.Select(s => s.IncludeDirs), this.Replacements);
	//			this.Args = Replace(projectParts.Select(s => s.Args), this.Replacements);
	//			this.CompileArgs = Replace(projectParts.Select(s => s.CompileArgs), this.Replacements);
	//			this.LinkArgs = Replace(projectParts.Select(s => s.LinkArgs), this.Replacements);
	//		}

	//		protected override BuildResults BuildImpl()
	//		{
	//			var allCppFiles = this.SrcDirs.SelectMany(d => FileUtils.FindFiles(d, new string[] { ".c", ".cpp" }))
	//				.Select(cppFile =>
	//				{
	//					var oDir = Path.GetRelativePath(this.ProjectDir, Path.GetDirectoryName(cppFile)!);
	//					var oName = Path.GetFileName(cppFile) + ".o";
	//					var oFile = Path.GetFullPath(Path.Join(this.BuildConfig.TempDir, this.RelativePath, this.ProjectName, oDir, oName));
	//					return (cppFile, oFile);
	//				}).ToArray();

	//			var filesToCompile = allCppFiles
	//				.Where(f =>
	//				{
	//					return FileUtils.YoungestFile(
	//						FileUtils.FindDeps(f.cppFile, this.IncludeDirectories).Union(new[] { f.cppFile, f.oFile, this.ProjectPath }).Union(this.LibFiles))
	//					 != f.oFile;
	//				}).ToArray();

	//			if (filesToCompile.Length > 0)
	//			{
	//				Console.WriteLine(string.Join("\n", filesToCompile.Select(f => $"compiling: {Path.GetRelativePath(this.ProjectDir, f.cppFile)}")));
	//				var errorHappened = false;
	//				Parallel.ForEach(filesToCompile, (f, _, __) =>
	//				{
	//					Directory.CreateDirectory(Path.GetDirectoryName(f.oFile)!);
	//					var process = new Process()
	//					{
	//						StartInfo = new ProcessStartInfo
	//						{
	//							WorkingDirectory = ProjectDir,
	//							FileName = f.cppFile.EndsWith(".c") ? "clang" : "clang++",
	//							Arguments =
	//								"-c " +
	//								$"{f.cppFile} " +
	//								$"{string.Join(" ", this.Args)} " +
	//								$"{string.Join(" ", this.CompileArgs)} " +
	//								$"-o {f.oFile} ",
	//						},
	//					};
	//					process.Start();
	//					process.WaitForExit();

	//					if (process.ExitCode != 0)
	//						errorHappened = true;
	//				});
	//				if (errorHappened)
	//					return BuildResults.Failed;
	//			}

	//			var libSearchPaths = this.LibFiles
	//				.Select(f => Path.GetDirectoryName(f))
	//				.ToHashSet();

	//			var libNames = this.LibFiles
	//				.Select(f => Path.GetFileNameWithoutExtension(f))
	//				.ToArray();

	//			var filesToLink = allCppFiles.Select(f => f.oFile).ToArray();
	//			if (FileUtils.YoungestFile(filesToLink.Append(this.DstAssembly)) != this.DstAssembly)
	//			{
	//				Console.WriteLine($"linking: {Path.GetRelativePath(this.ProjectDir, this.DstAssembly)}");
	//				Directory.CreateDirectory(Path.GetDirectoryName(this.DstAssembly)!);
	//				var process = new Process()
	//				{
	//					StartInfo = new ProcessStartInfo
	//					{
	//						WorkingDirectory = ProjectDir,
	//						FileName = "clang++",
	//						Arguments =
	//							$"{string.Join(" ", filesToLink)} " +
	//							$"{string.Join(" ", this.Args)} " +
	//							$"{string.Join(" ", this.LinkArgs)} " +
	//							$"{string.Join(" ", libSearchPaths.Select(p => $"-L \"{p}\""))} " +
	//							$"{string.Join(" ", libNames.Select(n => $"-l \"{n}\""))} " +
	//							$"-o {this.DstAssembly} ",
	//					}
	//				};
	//				process.Start();
	//				process.WaitForExit();
	//				return process.ExitCode != 0 ? BuildResults.Failed : BuildResults.Succeeded;
	//			}
	//			else
	//			{
	//				return BuildResults.Skipped;
	//			}
	//		}
	//	}

	internal static class ProjectPartBuildUtils
	{
		//public static T[] Filter<T>(this T[] projectParts, BuildConfig buildConfig) where T : ProjectPart =>
		//	projectParts
		//	.Where(p => p.Filter.Config == null || p.Filter.Config == buildConfig.Config)
		//	.Where(p => p.Filter.Platform == null || p.Filter.Platform == buildConfig.Platform)
		//	.ToArray();

		public static T? Combine<T>(T? a, T? b) =>
			b ?? a;
		public static T[]? Combine<T>(T[]? a, T[]? b) =>
			a != null && b != null ? a.Union(b).ToArray() : a ?? b;

		public static string[]? ReplaceVariables(this string[]? s, Dictionary<string, string> replacements)
		{
			if (s != null)
			{
				for (var i = 0; i < s.Length; i++)
					s[i] = s[i].ReplaceVariables(replacements)!;
			}
			return s;
		}

		public static string? ReplacePathVariables(this string? s, Dictionary<string, string> replacements)
		{
			if (s != null)
				s = Path.GetFullPath(s.ReplaceVariables(replacements)!);
			return s;
		}

		public static string[]? ReplacePathVariables(this string[]? s, Dictionary<string, string> replacements)
		{
			if (s != null)
			{
				for (var i = 0; i < s.Length; i++)
					s[i] = Path.GetFullPath(s[i].ReplaceVariables(replacements)!);
			}
			return s;
		}

		public static string Replace(IEnumerable<string?> strings, Dictionary<string, string> replacements) =>
			strings.Aggregate(Combine)?.ReplaceVariables(replacements) ?? string.Empty;

		public static string[] Replace(IEnumerable<string[]?> strings, Dictionary<string, string> replacements) =>
			strings.Aggregate(Combine).ReplaceVariables(replacements) ?? Array.Empty<string>();

		public static string ReplacePath(IEnumerable<string?> paths, Dictionary<string, string> replacements) =>
			paths.Aggregate(Combine).ReplacePathVariables(replacements) ?? string.Empty;

		public static string[] ReplacePath(IEnumerable<string[]?> paths, Dictionary<string, string> replacements) =>
			paths.Aggregate(Combine).ReplacePathVariables(replacements) ?? Array.Empty<string>();
	}
}
