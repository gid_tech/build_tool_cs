using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace BuildTool
{
	internal static class FileUtils
	{
		public static IEnumerable<string> FindFiles(string dir, string ending) =>
			FindFiles(dir, new[] { ending });

		public static IEnumerable<string> FindFiles(string dir, string[] endings)
		{
			foreach (var file in Directory.GetFiles(dir).Where(d => endings.Contains(Path.GetExtension(d))))
				yield return file;
			foreach (var subdir in Directory.GetDirectories(dir))
			{
				foreach (var file in FindFiles(subdir, endings))
					yield return file;
			}
		}

		private static readonly Regex includeRegex = new("#include ([\"<])(.*)[\">]");
		private static readonly Dictionary<string, List<string>> depsCache = new();

		public static IReadOnlyList<string> FindDeps(string file, string[] includePaths)
		{
			lock (depsCache)
			{
				if (!depsCache.TryGetValue(file, out var deps))
				{
					var fileDir = Path.GetDirectoryName(file);
					deps = new List<string>();
					depsCache[file] = deps;

					foreach (var line in File.ReadLines(file))
					{
						var matches = includeRegex.Matches(line);
						foreach (Match m in matches)
						{
							var type = m.Groups[1].Value;
							var include = m.Groups[2].Value;

							var found = false;
							var depPath = "";

							if (type == "\"")
							{
								depPath = Path.GetFullPath(Path.Join(fileDir, include));
								if (File.Exists(depPath))
								{
									found = true;
								}
							}
							if (!found)
							{
								foreach (var includePath in includePaths)
								{
									depPath = Path.GetFullPath(Path.Join(includePath, include));
									if (File.Exists(depPath))
									{
										found = true;
										break;
									}
								}
							}

							if (found)
							{
								deps.Add(depPath);
								deps.AddRange(FindDeps(depPath, includePaths));
							}
						}
					}
				}
				return deps;
			}
		}

		public static string YoungestFile(IEnumerable<string> files) =>
			files.OrderBy(f => File.GetLastWriteTime(f)).ToArray().Last();
	}
}
