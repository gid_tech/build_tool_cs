﻿using System;
using System.Runtime.InteropServices;

namespace BuildTool
{
	public enum Platform
	{
		Linux, Windows, MacOS
	}

	public static class PlatformUtil
	{
		public static Platform FromString(string s) {
			return s switch {
				"Linux" => Platform.Linux,
				"Widnows" => Platform.Windows,
				"MacOS" => Platform.MacOS,
				_ => throw new NotImplementedException(),
			};
		}

		public static readonly Platform Current = GetCurrent();

		private static Platform GetCurrent()
		{
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
				return Platform.Linux;
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
				return Platform.Windows;
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
				return Platform.MacOS;
			throw new NotImplementedException();
		}
	}
}
