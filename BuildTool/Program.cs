﻿using System;
using System.IO;
using System.Xml;

namespace BuildTool
{
	internal static class Program
	{
		private static void Main(string[] args)
		{
			if (args.Length == 4)
			{
				var projectPath = Path.GetFullPath(args[0]);
				var config = args[1];
				var targetDir = Path.GetFullPath(args[2]);
				var tempDir = Path.GetFullPath(args[3]);

				var buildConfig = new BuildConfig(config, tempDir, targetDir);

				var timer = new Timer(Path.GetFullPath("."));
				var task = new Timer.Task(projectPath, "total build");

				//var success = ProjectBuild.Build(projectPath, buildConfig, timer);
				var success = true;

				timer.Submit(task);
				Console.WriteLine("");
				timer.Print();

				if (!success)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine("\nbuild failed");
					Console.ResetColor();
				}
			}
			else
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(
					$"wrong usage: { string.Join(" ", args)}" +
					$"BuildTool projectPath config targetDir tempDir"
				);
				Console.ResetColor();
			}
		}
	}
}
