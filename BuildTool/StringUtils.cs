﻿using System;
using System.Collections.Generic;

namespace BuildTool
{
	internal static class StringUtils
	{
		public static string ReplaceVariables(this string s, Dictionary<string, string> replacements)
		{
			s = Environment.ExpandEnvironmentVariables(s);
			foreach (var (from, to) in replacements)
				s = s.Replace($"{{{from}}}", to);
			return s;
		}
	}
}
