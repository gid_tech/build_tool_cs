﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BuildTool
{
	public class Timer
	{
		private readonly string pathRoot;
		private readonly List<(PojectData, TimeSpan)> finished = new();

		public Timer(string pathRoot)
		{
			this.pathRoot = pathRoot;
		}

		public void Submit(Task task)
		{
			lock (this.finished)
				this.finished.Add((task.data, DateTime.Now - task.startTime));
		}

		public void Print()
		{
			lock (this.finished)
			{
				Console.WriteLine();
				foreach (var (data, time) in this.finished)
					Console.WriteLine($"{time} time {data.Activity}: {Path.GetRelativePath(this.pathRoot, data.Project)}");
			}
		}

		public readonly struct PojectData
		{
			public readonly string Project;
			public readonly string Activity;

			public PojectData(string project, string activity)
			{
				this.Project = project;
				this.Activity = activity;
			}
		}

		public struct Task
		{
			public readonly DateTime startTime;
			public readonly PojectData data;

			public Task(string project, string activity)
			{
				this.startTime = DateTime.Now;
				this.data = new PojectData(project, activity);
			}
		}
	}
}
