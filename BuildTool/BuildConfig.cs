﻿using System.Collections.Generic;
using System.IO;

namespace BuildTool
{
	public struct BuildConfig
	{
		public readonly Platform Platform;
		public readonly string Config;
		public readonly string TempDir;
		public readonly string TargetDir;

		public BuildConfig(string config, string tempDirectory, string targetDirectory)
		{
			this.Platform = PlatformUtil.Current;
			this.Config = config;

			string ReplacePath(string path) =>
				path.ReplaceVariables(new Dictionary<string, string>
				{
					{ "Platform", PlatformUtil.Current.ToString().ToLower() },
					{ "Configuration", config }
				})!;

			this.TempDir = Path.GetFullPath(ReplacePath(tempDirectory));
			this.TargetDir = Path.GetFullPath(ReplacePath(targetDirectory));
		}
	}
}
